
class bot {

    public void principal(){
        int k = 0;
        while(k<4){
            k = SimpleInput.getInt("Combien de pièces pour le jeu ? minimum de 5 pour jouer pour que la partie ne sois pas longue : ");
        }
        String p1 = SimpleInput.getString("Nom joueur 1 : ");
        int p2 = SimpleInput.getInt("Rentres 1 pour joueur, 0 pour ordinateur ");

        partie(p1, p2, creation(k));     
    }

    String[][] creation(int k){// méthode qui va créer les tableauxx utilisés dans l'affichage, le premier tableau étant remplis de batons et tous faisant la longueur du nombre de batons rentrée au début
        String[][] tableau;
        tableau = new String[k][k];
        for (int i = 0; i < tableau.length; i++) {
           for (int j = 0; j < tableau.length; j++) {
              if(i == 0){
                tableau[i][j] = "|";
              } else {
                tableau[i][j] = "0";
              }
           }
        }
        System.out.println();

        return tableau;
    }

    void test_creation(){
        test_cas_creation(4);
        test_cas_creation(0);
        test_cas_creation(-5);


    }

    void test_cas_creation(int k){
        System.out.println("Pour k = " + k);
        if (k <= 0){
            System.out.println("On ne peut pas faire de tableaux négatifs ou nuls, cela ne nous permet pas de jouer");
        }        
        System.out.println("Ok, cela fonctionne");
    }
    int choix_baton_bot(String[][] tab, int ligne, int coup, int nb_batons, int pred){
        int choix_nb = 0;
        if(nb_batons == 3){        
 
            choix_nb = 2;
        }else if(nb_batons == 4){
            choix_nb = 3;
        }else if(coup == 0){
            if(pred %2 == 0){
                if(nb_batons%2 == 0){
                    choix_nb = nb_batons -1;
                }else{
                    choix_nb = nb_batons -2;
                }
            }else{

                if(nb_batons%2 == 0){

                    choix_nb = nb_batons -2;
                }else{

                    choix_nb = nb_batons -3;
                }
            }
        }
        
        return choix_nb;
    }


    void test_baton(){
        String[][]tab5 = {};

        String[][]tab3 = {{"|", "|", "|", "k"}, {"|","|","k","iokhv"}};
        String[][]tab2 = {{"|", "|", "|", "k"}, {"|","|","k","iokhv"}};
        String[][]tab4 = {{"|", "|", "|", "k"}, {"|","|","k","iokhv"}};
        String[][]tab1 = {{"|", "|", "|", "k"}, {"|","|","k","iokhv"}};
        test_cas_baton(tab1, 0, 0, 0, -1);
        test_cas_baton(tab2, 0, 0, -1, 0);
        test_cas_baton(tab3, 3, 0, 0, 0);
        test_cas_baton(tab4, 0, 0, 0, 0);
        test_cas_baton(tab5, 0, 0, 0, 0);


    }

    void test_cas_baton(String[][] tab, int ligne, int coup, int nb_batons, int pred){
        System.out.println("Test de la méthode : baton_bot ");
        System.out.println("");
        if(tab.length == 0){
            System.out.println(" Erreur Le tableau doit être remplit");

        }else if(coup!= 0 || coup !=1){
            System.out.println("Erreur Les coups sont forcément de 0 1 ou 2");
        }else if(ligne > tab.length){
            System.out.println("Erreur On ne peut pas aller chercher une ligne plus loins que dans le tableau");
        }else if(0 > pred || 0 > nb_batons){
            System.out.println("Erreur On ne peut pas avoir de nombre négatifs");
        }else{
            System.out.println("Ok Le programme fonticonne");
        }
    }

    int[] choix_ligne_bot(String[][] tab){
        int prediction_tour_restants_minimum = 0;
        int i = 0;
        boolean fdj = true;
        int coup_a_faire = 0;
        int[] solution;
        solution = new int[4];
        while(tab.length > i){
            int j= 0;
            int nb_bat = 0;
            while(tab.length > j){
                if(tab[i][j] =="|"){
                    nb_bat++;
                }
                j++;
            }
            if(nb_bat == 0 || nb_bat == 1 || nb_bat == 2){
                prediction_tour_restants_minimum = prediction_tour_restants_minimum +0;
            } else if(nb_bat == 3){

                prediction_tour_restants_minimum++;//car on sait que quand il y a un 3 il ne peut que se décomposer en 1 et en 2, donc un seul coup
            } else if(nb_bat == 4){
                prediction_tour_restants_minimum = prediction_tour_restants_minimum +2;// pareil que pour 3 on sait que 4 ne peut se décommposer que en 3 et 1 donc il reste 2 coups : 1 et 3 puis 1 1 et 2 par exemple
            }else{
                fdj = false;
                // on ne sait pas le nombre de coups restants pour le même nombre de batons pas ligne, pour 5 celà peut être 2 coups comme 3, donc la valeur sera définie aléatoirement par le joueur
                //mais pas par le bot, car celui ci va jouer en fonctione du nombre de tours restants possibles, essayant des fois de faire passer des lignes à d'autres lignes avec un nombre de coups définis
            }
            i++;
        }
        if(fdj == true){
            coup_a_faire = 1;
        }else{
            coup_a_faire = 0;
        }

        solution[0] = coup_a_faire;
        solution[1] = (choix_lgn(coup_a_faire, prediction_tour_restants_minimum, tab)[0]);
        solution[2] = (choix_lgn(coup_a_faire, prediction_tour_restants_minimum, tab)[1]);
        solution[3] = prediction_tour_restants_minimum;
        return(solution);

    }

    void test_ba_ln(){
        String[][]tab5 = {};
        String[][]tab3 = {{"|", "|", "|", "k"}, {"|","|","k","iokhv"}};
        test_cas_ln(tab3);
        test_cas_ln(tab5);

    }

    void test_cas_ln(String[][] tab){
        System.out.println("Test de la méthode  : choix ligne bot");
        System.out.println("");
        if(tab.length == 0){
            System.out.println(" Erreur Un tableau ne peut pas être vide pour le jeu");
        }else{
            System.out.println("Ok le programme fonctionne");
        }
    }

    int[] choix_lgn(int caf, int ptrm, String[][] tab){// dans cette méthode nous alons chercher à prendre la bonne ligne en fonctione de plusieurs critères comme la taille du tableau et le nombre de batons dans le tableau
        int choix = 0;
        int nb_bat =0;
        int[] solution;
        solution = new int[2];
        if(caf == 1){// si on connait le nombre de tours restants
            int i = 0;
            while(tab.length > i){
                int j = 0;
                int nb_ba = 0;
                while(tab.length> j){
                    if(tab[i][j] == "|"){
                        nb_ba++;
                        if(nb_ba == 3 || nb_ba == 4){
                            choix = i;
                            nb_bat = nb_ba;// nombre de batons à enlever par le bot


                        }
                    }
                    j++;
                }
                i++;
            }
        } else {// si on ne connait pas le nombre de tours restants
            int i = 0;

            while(tab.length > i){
                int j = 0;
                int nb_ba = 0;
                while(tab.length> j){
                    if(tab[i][j] == "|"){
                        nb_ba++;
                        if(nb_ba > 4){
                            choix = i;
                            nb_bat = nb_ba;

                        }
                    }
                    j++;
                }
                i++;
            }

        }


        solution[0] = choix;// ligne
        solution[1] = nb_bat;// nombre de batons dans la ligne
        return solution;
    }

    void test_choix_lgn(){// test de a méthode choix_lgn
        String[][]tab5 = {};
        String[][]tab3 = {{"|", "|", "|", "k"}, {"|","|","k","iokhv"}};
        test_cas_lgn(0, 0, tab5);
        test_cas_lgn(0, -1, tab3);
        test_cas_lgn(4, 0, tab3);


    }

    void test_cas_lgn(int caf, int ptrm, String[][] tab){ 
        System.out.println("Test de la méthode choix_ligne : ");
        System.out.println("");
        if(tab.length == 0){
            System.out.println(" Erreur Le tableau doit être remplit");

        }else if(caf!= 0 || caf !=1 || caf !=2){
            System.out.println("Erreur Les coups sont forcément de 0 1 ou 2");

        }else if(0 > ptrm){
            System.out.println("Erreur On ne peut pas avoir de nombre négatifs");
        }else{
            System.out.println("Ok Le programme fonticonne");
        }
    }

    void afficherjeu(String[][] tab, int k){// méthode qui sera appelée plusieurs fois au cours de la partie pour afficher les tableaux en cours dans la partie
        System.out.println(" Affichage du jeu : ");
        for (int i = 0;i <= k; i++){
            System.out.print(" Ligne " + i + " : ");
            for (int l = 0;l < tab[i].length; l++){
                if(tab[i][l] == "|"){
                    System.out.print(" | ");
                }

            } 
            System.out.println("");
            System.out.println("");

        }
 
    }

    void test_jeu(){//méthodes de test pout afficherjeu
        String[][]tab1 = {{"o"}};
        int k = 5;
        test_cas_jeu(tab1, k);
        String[][]tab2 = {{}};
        int j = 0;
        test_cas_jeu(tab2, j);
        String[][]tab3 = {{"|", "|", "|", "k"}, {"|","|","k","iokhv"}};
        int m = 5;
        test_cas_jeu(tab3, m);
        String[][]tab4 = {{"|", "|", "|", "k"}, {"|","|","k","iokhv"}};
        int o = 2;
        test_cas_jeu(tab4, o);
        String[][]tab5 = {{"|", "|", "|", "k"}, {"|","|","k","iokhv"}};
        int p = 1;
        test_cas_jeu(tab5, p);

    }

    void test_cas_jeu(String[][] tab, int k){
        System.out.println("Test de la méthode : afficherjeu ");
        System.out.println("");
        System.out.println("test afficher jeu pour k :" + k + "Et pour un tableau de longueur : " + tab.length);
        if(tab.length <= 0 || k <= 0){
            System.out.println("Erreur On ne peut pas créer de tableaux vide sou négatifs");
        } else if(k > tab.length) {
            System.out.println("Erreur On ne peut pas afficher de tableaux ou la longueur demandée est plus grande que celle du tableau");

        } else  {
            System.out.println("Ok Cela fonctionne");

        }
    }
  

    void enlever_baton(String[][] tab, int nb, int nbe, int tour){ // méthode qui sera utilisée pour enlever les batons  dans un tableaux donné et les rajouter dans la tableaux qui sera ajouté juste après
        int j = 0;
        while (nb>j){// méthode pour rajouter les batons
            tab[tour+1][j] = "|";
            j = j+1;

        }


        int u = 0;
        int y = 0;
        while(nb >y ){
            if(tab[nbe][u] == "|"){ // méthode pour enlever les batons et les remplacer par des 0 qui ne s'afficheront pas  dans le tableau ou nous voulons enlever les batons
                tab[nbe][u] = "0";
                y++;
            }
            u = u+1;

        }
    }
    void test_batonn(){
        String[][]tab1 = {{"|", "|", "|", "k"}, {"|","|","k","iokhv"},{"0","0","0","0"}};
        test_cas_enlever_baton(tab1, 0, 0, 10);
        String[][]tab2 = {{"|", "|", "|", "k"}, {"|","|","k","iokhv"},{"0","0","0","0"}};
        test_cas_enlever_baton(tab2, 5, 0, 1);

        String[][]tab3 = {{"|", "|", "|", "k"}, {"|","|","k","iokhv"},{"0","0","0","0"}};
        test_cas_enlever_baton(tab3, 0, 5, 1);

        String[][]tab4 = {{"|", "|", "|", "k"}, {"|","|","k","iokhv"},{"0","0","0","0"}};// test qui fonctionne
        test_cas_enlever_baton(tab4, 0, 0, 1);

        String[][]tab5 = {{"|", "|", "|", "k"}, {"|","|","k","iokhv"},{"0","0","0","0"}};
        test_cas_enlever_baton(tab5, 0, 0, 1);



    }

    void test_cas_enlever_baton(String[][] tab, int a, int b, int c){
        System.out.println("Test de la méthode : enlever baton ");
        System.out.println("");
        System.out.println("Pour enlever " + a +  " batons, du tableau " + b + " vers le tableau " + c + " : " );
        if(c>tab.length || b >= tab.length){
            System.out.println("ErreurOn ne peut pas remplacer des nombres dans un tableau inexistant");
        } else if(a>=tab.length){
            System.out.println("Erreur La longueur du tableau de tableau demandé est insuffisante");
        }else if(a == b-1){
            System.out.println("Erreur On ne peut pas faire d'échanges de baton sur la même ligne");
        }else {
            System.out.println("Ok, cela Fonctionne");
        }
    }


    boolean possible_remplir(String[][] tab, int bat_changer, int tour){ // méthode qui est utilisée dans le but de savoir si il est possible d'enlever un nombre de batons donnés dans la ligne ou nous voulons les enlever, renvoie un boolean 
        int i = 0;
        int tot = 0;
        boolean remplir = false;

        while(tab[tour].length > i){// compte le nombre de batons dans la ligne ou nous voulons les enlever
            if(tab[tour][i] == "|"){
                tot ++;
            }
            i = i+1;
        }


        if(tot > bat_changer){// verifie si le nombre de batons est supérieur à celui que l'on veut enlever à cette ligne
            remplir = true;
        }else{
            System.out.println("Le nombre rentré dépasse ou est égal au nombre d'alumettes dans cette ligne ! ");
        }
        if(bat_changer % 2 == 0 && bat_changer * 2 == tot ){// si le nombre choisit est la moitié du nombre que l'on veut enlever cela renvoit faux : exemple 4 dans une lgne, on veut en enlever 2 ça nous return false
            System.out.println("On n'a pas le droit de rentrer un nombre qui est la moitié du nombre d'alumettes dans cette ligne ! ");
            remplir = false;
        }
        return remplir;
    }

    
    void test_possible(){
        String[][]tab1 = {{"|", "|", "|", "k"}, {"|","|","k","iokhv"},{"0","0","0","0"}};// test quifonctionne et rencera true
        cas_test_possible(tab1, 2, 0);
        String[][]tab2 = {{"|", "|", "|", "k"}, {"|","|","k","iokhv"},{"0","0","0","0"}};// test quifonctionne et rencera false
        cas_test_possible(tab2, 3, 1);

        String[][]tab3 = {};//premier if, tableau vide
        cas_test_possible(tab3, 0, 0);

        String[][]tab4 = {{"|", "|", "|", "k"}, {"|","|","k","iokhv"},{"0","0","0","0"}};//deuxième if, il n'y a pas d'indice 5 
        cas_test_possible(tab4, 0, 5);

        String[][]tab5 = {{"|", "|", "|", "k"}, {"|","|","k","iokhv"},{"0","0","0","0"}};//troisième if, on ne peut pas enlever 5 batons quand la longueur du tableau de tableau n'est que de 4
        cas_test_possible(tab5, 5, 0);
    }

    void cas_test_possible(String[][] tab, int bat_changer, int tour){
        System.out.println("Test de la méthode possible remplir : ");
        System.out.println("");
        if(tab.length == 0){
            System.out.println("Erreur Tableau vide, celà ne fonctionne pas");
        } else if(tour > tab.length){
            System.out.println("Erreur On ne peut pas modifier une ligne alors que elle n'existe pas");
        } else if (bat_changer >=  tab[tour].length){
            System.out.println("Erreur On ne peut pas enlever" + bat_changer + "dans un tableau qui a une longueur plus petite ou égale que" + bat_changer);
        } else {
            System.out.println("Ok, cela fonctionne");
        }

    }
    boolean uni(String[][] tab, int tour){// compte le nombre de batons dans une ligne, si celui ci est nul de 1 ou 2 cela nous demandera d'à nouver rentrer un nombre
        int i = 0;
        int total = 0;
        boolean val = true;
        while(tab[tour].length > i){//compte le nombre de batons
            if(tab[tour][i] == "|"){
                total = total +1;
            }
            i = i+1;
        }

        if(total == 0 || total == 1 || total == 2){//regarder si il y a 0 1 ou 2 batons dans la ligne
            val = false;
        }
        return val;
    }
    void test_uni(){
        String[][]tab1 = {{"|", "|", "|", "k"}, {"|","|","k","iokhv"},{"0","0","0","0"}};
        test_cas_uni(tab1, 2);
        String[][]tab2 = {{"|", "|", "|", "k"}, {"|","|","k","iokhv"},{"0","0","0","0"}};
        test_cas_uni(tab2, 3);


    }

    void test_cas_uni(String[][] tab, int tour){
        System.out.println("Test de la méthode : uni ");
        System.out.println("");
        if(tab.length == 0){
            System.out.println("Erreur Tableau vide, celà ne fonctionne pas");
        } else if(tour > tab.length){
            System.out.println("Erreur On nous dit que il y a " + tour + " batons " + "alors que la longueur du tableau est plus petite que le nombre de baton que l'on demande d'enlever à cette ligne");

        } else {
            System.out.println("Ok, cela fonctionne");
        }
    }

    boolean fini(String[][] tab){// méthode qui comme la plupart va s'éxécuter durant chaque tour, elle va vérifier si la longueur de tous les tableaux sont soit nul ou avec 1 ou 2 batons, ce ui va arrêter le jeu et désigner le gagnant
        boolean fin = true;
        int i =0;
        while(tab.length > i && fin == true){//boucle qui va parcourir le tableau
            int j = 0;
            int tot = 0;
            while(j < tab[i].length && fin == true){//boucle qui va parcourir tous les tableaux de tableaux et compter le nombre d'alumettes
                if(tab[i][j] == "|"){
                    tot++;
                    if(tot >= 3){
                        fin = false;
                    }
                }
                j++;
            }
            i++;
        }
        return fin;
    }

    void test_fini(){// test de méthode fini
        System.out.println("Test de la méthode uni");
        String[][]tab1 = {{"|", "|", "|", "k"}, {"|","|","k","iokhv"},{"0","0","0","0"}};// test qui fonctionne et renverra false, ce qui ne finira pas le jeu 
        test_cas_fini(tab1);
        String[][]tab2 = {{"|", "|", "m", "k"}, {"|","|","k","iokhv"},{"0","0","0","0"}};// test qui fonctionne et renverra true, ce qui finira le jeu 
        test_cas_fini(tab2);


    }

    void test_cas_fini(String[][] tab){
        System.out.println("Test de la méthode fini: ");
        System.out.println("");
        System.out.println("Erreur Pour ce tableau peut-on voir si il peut permettre une fin de partie ? ");
        if(tab.length == 0){
            System.out.println("Erreur Non, on ne peut pas jouer avec aucun baton et tableau");

         

        } else {
            int total = 0;
            int i = 0;
            while(tab.length > i){
                int j = 0;
                while(tab[i].length > j){
                    if(tab[i][j] == "|"){
                        total++;
                    }
                    j++;
                }
                i++;
            }
            if(total == 0){
                System.out.println("Erreur Non, Il n'y a pas de batons dans aucun des tableaux, je jeu ne peut pas fonctionner");
            } else {
                System.out.print("Ok  le test peut se faire");
            }
        }
}



    void partie(String p1, int p2, String[][] tab){//méthode principale qui va faire tourner le jeu et appeler la plupart des autres méthodes.
        int nb_tour = 0;
        afficherjeu(tab, nb_tour);//affiche le jeu
        while(tab.length > nb_tour){// la boucle va s'éxécuter un nombre x de tours
            System.out.print("Tour du joueur " + p1 + " ");//on choisit quelle ligne changer et on verifie si celà est possible grâce à la méthode uni
            int tour = SimpleInput.getInt(" Quelle ligne veut tu changer ? ");

            while(tour < 0 || tour > nb_tour || uni(tab, tour) == false){
                tour = SimpleInput.getInt("Quelle ligne veut tu changer , tu ne peux pas changer? ");
            }
            int bat_changer = SimpleInput.getInt("Combien de batons veux tu bouger ? ");//on choisit quelle nombre d'alumettes on veut changer et on verifie si celà est possible grâce à la méthode possible remplir
            while(bat_changer <= 0 || bat_changer >= tab[0].length || possible_remplir(tab, bat_changer, tour)==false){
                if(bat_changer < 0){
                    System.out.println("Le nombre rentré est négatif ou nul, tu ne peux pas faire ça ");//on va vérifier si le nombre n'est pas négatif ou trop grand
                }

                if(bat_changer >= tab[0].length){
                    System.out.println("Le nombre rentré est plus grand ou égal que la longueur maximum d'alumettes ! ");
                }
                bat_changer = SimpleInput.getInt("Rerentres un nombre : ");
            }
            enlever_baton(tab, bat_changer, tour, nb_tour);// on enlève les batons dans une ligne défunut grâce à la méthode enlever_baton
            if(fini(tab)){// on vérifie si le jeu est finit, donc si toutes les lignes contiennent 0 1 ou 2 alumettes
                System.out.println("victoire du joueur : " + p1);
                break;
            }
            System.out.println("");
            afficherjeu(tab, nb_tour+1);// affiche le jeu et passe au joueur suivant
            nb_tour ++;


            System.out.print("Tour du joueur " + p2);//on choisit quelle ligne changer et on verifie si celà est possible grâce à la méthode uni
            if(p2 == 0){
                bat_changer = choix_baton_bot(tab, choix_ligne_bot(tab)[1], choix_ligne_bot(tab)[0], choix_ligne_bot(tab)[2], choix_ligne_bot(tab)[3]);//0 coup 1 ligne 2 nb batons
                enlever_baton(tab, bat_changer, choix_ligne_bot(tab)[1], nb_tour);// on enlève les batons dans une ligne défunut grâce à la méthode enlever_baton
    
                System.out.println("");
                afficherjeu(tab, nb_tour+1);// affiche le jeu et passe au joueur suivant
                if(fini(tab)){// on vérifie si le jeu est finit, donc si toutes les lignes contiennent 0 1 ou 2 alumettes
                    System.out.println("victoire du joueur : bot");
                    break;
                }
                nb_tour ++;
            }else{
                System.out.println("Tour du joueur " + p2 + " ");//on choisit quelle ligne changer et on verifie si celà est possible grâce à la méthode uni
                tour = SimpleInput.getInt(" Quelle ligne veut tu changer ? ");
    
                while(tour < 0 || tour > nb_tour || uni(tab, tour) == false){
                    tour = SimpleInput.getInt("Quelle ligne veut tu changer , tu ne peux pas changer? ");
                }
                bat_changer = SimpleInput.getInt("Combien de batons veux tu bouger ? ");//on choisit quelle nombre d'alumettes on veut changer et on verifie si celà est possible grâce à la méthode possible remplir
                while(bat_changer <= 0 || bat_changer >= tab[0].length || possible_remplir(tab, bat_changer, tour)==false){
                    if(bat_changer < 0){
                        System.out.println("Le nombre rentré est négatif ou nul, tu ne peux pas faire ça ");//on va vérifier si le nombre n'est pas négatif ou trop grand
                    }
    
                    if(bat_changer >= tab[0].length){
                        System.out.println("Le nombre rentré est plus grand ou égal que la longueur maximum d'alumettes ! ");
                    }
                    bat_changer = SimpleInput.getInt("Rerentres un nombre : ");
                }
                enlever_baton(tab, bat_changer, tour, nb_tour);// on enlève les batons dans une ligne défunut grâce à la méthode enlever_baton
                if(fini(tab)){// on vérifie si le jeu est finit, donc si toutes les lignes contiennent 0 1 ou 2 alumettes
                    System.out.println("victoire du joueur : " + p2);
                    break;
                }
                System.out.println("");
                afficherjeu(tab, nb_tour+1);// affiche le jeu et passe au joueur suivant
                nb_tour ++;
    
        
            }

        }
    }

    void test_partie(){
        String[][]tab1 = {{"|", "|", "|", "k"}, {"|","|","k","iokhv"},{"0","0","0","0"}};// test qui ne fonctionnera pas car on a des batons autre que dans le premier tableau
        testCas_partie(tab1, "oui", "non");
        String[][]tab2 = {{"|", "|", "|", "k"}, {"i","l","k"},{"0","0","0","0"}};// test qui ne fonctionne pas car les tableaux de tableaux ne sont pas tous de la même taille
        testCas_partie(tab2, "oui", "non");

        String[][]tab3 = {};//tableau vide, celà ne fonctionne pas
        testCas_partie(tab3, "oui", "non");

        String[][]tab4 = {{"y", "0", "p", "k"}, {"o","m","k","iokhv"},{"0","0","0","0"}};//On  ne peut pas jouer sans batons
        testCas_partie(tab4, "oui", "non");

        String[][]tab5 = {{"|", "|", "|", "k"}, {"|","|","k","iokhv"},{"0","0","0","0"}};// Les pseudos sont vides, celà ne fonctionnera pas
        testCas_partie(tab5, "", "non");
        String[][]tab6 = {{"|", "|", "|", "k"}, {"0","0","k","iokhv"},{"0","0","0","0"}};// Il manque toujours un minimum de 5 batons dans le premier tabeau
        testCas_partie(tab6, "oui", "non");
        String[][]tab7 = {{"|", "|", "|", "|", "|","|"}, {"0","0","k","iokhv","j","kk"},{"0","0","0","0","0","0"}};// Il manque toujours un minimum de 5 batons dans le premier tabeau
        testCas_partie(tab7, "oui", "non");





    }
    void testCas_partie (String[][] tab, String p1, String p2) {//test de la partie
        System.out.println("Test de la méthode : partie");
        System.out.println("");
        if(p1 == "" || p2 == ""){
            System.out.println("Erreur Non, le jeu ne peut pas se faire si un des deux joueurs n'ont pas de noms");
        } else if(tab.length == 0){
            System.out.println("Erreur On ne peut pas jouer avec un tableau vide");
        } else {
            boolean taille_tableau_egaux = true;
            int total = 0;
            int i = 0;
            while(tab.length > i && taille_tableau_egaux == true){
                int longueur = tab[0].length;
                int longueur_tableau_suivant = tab[i].length;
                int j = 0;
                if(longueur == longueur_tableau_suivant){
                    taille_tableau_egaux = false;
                }
                while(tab[i].length > j && taille_tableau_egaux == true){
                    if(tab[i][j] == "|"){
                        total++;
                    }
                    j++;
                }
                i++;
            }

            if(total < 5){
                System.out.println("Erreur On ne peut pas faire une partie avec moins de 5 batons, sinon elle se finirait en 1 ou deux coups forcément, donc il n'y a pas d'intéret de jouer");
            } else if(total == 0){
                System.out.println("Erreur On ne peut pas jouer sans batons");
            }else if (taille_tableau_egaux == false){
                System.out.println("Erreur Si la longueur des tableaux de tableaux sont différents, le jeu ne peut pas fonctionner car on ne pourra pas mettre par exemple 5 batons dans un tableau n'ayant que 3 de longueur");
            } else {
                boolean baton = true;
                int k = 1;
                while(tab.length > k){
                    int j = 0;
                    while(tab[k].length > j){
                        if(tab[k][j] == "|"){
                            baton = false;
                        }
                        j++;
                    }
                    k++;
                }
                if(baton == false){
                    System.out.println("Erreur On ne peut pas commencer le jeu avec des batons autre que dans le premier tableau/ligne, il faut que tous les batons soient dans la première ligne");
                } else {
                    System.out.println("Ok la partie peut se lancer,");
                }
                
            }
        }
    }

    void test_test(){
        //test_creation();
        //test_fini();
        //test_jeu();
        //test_partie();
        //test_baton();
        //test_possible();

    }




}

